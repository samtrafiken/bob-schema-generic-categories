# BoB schema generic-categories

If any property regarding generic category _fareClass_ is used in the [BoB Product API](https://samtrafiken.atlassian.net/wiki/spaces/BOB/pages/2042462293/BoB+Product+v3+OpenAPI) then it is RECOMMENDED to use the values specified in [fare-class.json](https://bitbucket.org/samtrafiken/bob-schema-generic-categories/src/master/fare-class.json)

If any property regarding generic category _transportMode_ is used in the [BoB Product API](https://samtrafiken.atlassian.net/wiki/spaces/BOB/pages/2042462293/BoB+Product+v3+OpenAPI) then is RECOMMENDED to use the values specified in [transport-mode.json](https://bitbucket.org/samtrafiken/bob-schema-generic-categories/src/master/transport-mode.json)

This is a part of the [BoB - National Ticket and Payment Standard](https://bob.samtrafiken.se), hosted by Samtrafiken i Sverige AB